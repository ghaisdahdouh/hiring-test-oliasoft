import React from "react";
import { Routes, Route, useNavigate, useLocation } from "react-router-dom";
import { TopBar } from "@oliasoft-open-source/react-ui-library";
import Logo from "client/views/images/logo@2x.png";

import { Main } from "client/views/main/main";
import { OilRigs } from "../components/oil-rigs/oil-rigs";
import Details from "../components/details/Details";
import Chart from "../components/chart/Chart";

export const App = () => {
  const navigate = useNavigate();
  const location = useLocation();

  const contentLinks = [
    {
      type: "Link",
      label: "Oil Rigs",
      active: location.pathname === "/oilrigs",
      onClick: () => {
        navigate("/oilrigs");
      },
    },
  ];

  return (
    <>
      <TopBar
        title={{
          logo: <img src={Logo} alt="logo" />,
          label: "Hiring Challenge",
        }}
        content={contentLinks}
      />
      <Routes>
        <Route path="/" element={<Main />} />
        <Route path="/oilrigs" element={<OilRigs />} />
        <Route path="/Details/:siteId" element={<Details />} />
        <Route path="/chart" element={<Chart />} />
      </Routes>
    </>
  );
};
