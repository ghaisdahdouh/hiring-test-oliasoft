import React, { useEffect, useState } from "react";
import { connect } from "react-redux";
import {
  Button,
  Card,
  Heading,
  Column,
  Row,
  Spinner,
} from "@oliasoft-open-source/react-ui-library";
import { oilRigsLoaded } from "store/entities/oil-rigs/oil-rigs";
import styles from "./oil-rigs.module.less";
import { sortBy } from "lodash";

const OilRigs = ({ list, loading, oilRigsLoaded }) => {
  const [oilRigs, setOilRigs] = useState(list);

  useEffect(() => {
    if (list) {
      setOilRigs(list);
    }
  }, [list]);

  return (
    <div className={styles.oilRigsPage}>
      <Card heading={<Heading>List of oil rigs ({oilRigs.length})</Heading>}>
        <Row>
          <Column width={200}>
            <Button
              label="Load oil rigs"
              onClick={oilRigsLoaded}
              loading={loading}
              disabled={loading}
            />
            <Button
              label="Sort oil rigs"
              onClick={() => setOilRigs(sortBy(oilRigs, "name"))}
              loading={loading}
              disabled={loading}
            />
          </Column>
          <Column>
            <div className={styles.oilRigsList}>
              {loading && (
                <div style={{ padding: 20 }}>
                  <Spinner dark />
                </div>
              )}
              {oilRigs.length ? (
                <ul>
                  {oilRigs.map((oilRig, i) => (
                    <li key={i}>{oilRig.id}</li>
                  ))}
                </ul>
              ) : (
                <em>None loaded</em>
              )}
            </div>
          </Column>
        </Row>
      </Card>
    </div>
  );
};

const mapStateToProps = ({ entities }) => {
  const { oilRigs } = entities;
  return {
    loading: oilRigs.loading,
    list: oilRigs.list,
  };
};

const mapDispatchToProps = {
  oilRigsLoaded,
};

const ConnectedOilRigs = connect(mapStateToProps, mapDispatchToProps)(OilRigs);
export { ConnectedOilRigs as OilRigs };
