import React, { useEffect, useState } from "react";
import { connect } from "react-redux";
import { BarChart, Bar, XAxis, YAxis, Tooltip, CartesianGrid } from "recharts";
import { useNavigate } from "react-router-dom";
import { Button } from "@oliasoft-open-source/react-ui-library";
import { sitesLoaded } from "store/entities/sites/sites";

const Chart = ({ sites, loading, sitesLoaded }) => {
  const navigate = useNavigate();

  useEffect(() => {
    if (!sites || sites.length === 0) {
      sitesLoaded();
    }
  }, [sitesLoaded, sites]);

  const data = sites.map((site) => ({
    name: site.name,
    oilRigs: site.oilRigs.length,
  }));

  return (
    <div style={{ paddingTop: 100 }}>
      <Button basic label="Back to Main View" onClick={() => navigate("/")} />
      <BarChart width={700} height={300} data={data}>
        <XAxis dataKey="name" />
        <YAxis />
        <Tooltip />
        <CartesianGrid strokeDasharray="3 3" />
        <Bar dataKey="oilRigs" fill="#8884d8" />
      </BarChart>
    </div>
  );
};

const mapStateToProps = (state) => {
  return {
    sites: state.entities.sites.list,
    loading: state.entities.sites.loading,
  };
};

const mapDispatchToProps = {
  sitesLoaded,
};

export default connect(mapStateToProps, mapDispatchToProps)(Chart);
