import React, { useEffect, useState } from "react";
import { connect } from "react-redux";
import {
  Button,
  Card,
  Heading,
  Column,
  Row,
  Spinner,
} from "@oliasoft-open-source/react-ui-library";
import { sitesLoaded } from "store/entities/sites/sites";
import styles from "./sites.module.less";
import { sortBy } from "lodash";
import { useNavigate } from "react-router-dom";

const Sites = ({ list, loading, sitesLoaded }) => {
  const navigate = useNavigate();

  const [sites, setSites] = useState(list);

  const sortSites = () => setSites(sortBy(sites, "name"));

  useEffect(() => {
    if (list) {
      setSites(list);
    }
  }, [list]);

  return (
    <div className={styles.sitePage}>
      <Button basic label="View Chart" onClick={() => navigate("/chart")} />

      <Card heading={<Heading>List of oil sites ({sites.length})</Heading>}>
        <Row>
          <Column width={200} style={{ margin: 12, border: "2px solid red" }}>
            <Button
              label="Load sites"
              onClick={sitesLoaded}
              loading={loading}
              disabled={loading}
            />
            <Button
              label="Sort Sites"
              onClick={() => setSites(sortBy(sites, "name"))}
              loading={loading}
              disabled={loading}
            />
          </Column>
          <Column>
            {loading && (
              <div style={{ padding: 20 }}>
                <Spinner dark />
              </div>
            )}
            {sites.length ? (
              <ul style={{ display: "flex", flexWrap: "wrap", gap: 20 }}>
                {sites.map((site) => (
                  <li
                    key={site.id}
                    onClick={() => navigate(`/Details/${site.id}`)}
                    className={styles.siteItem}
                  >
                    <strong>Name:</strong> {site.name}
                    <br />
                    <strong>Country:</strong> {site.country}
                    <br />
                    <strong>Oil Rigs:</strong>
                    <ul>
                      {site.oilRigs.map((rigId) => (
                        <li key={rigId}>{rigId}</li>
                      ))}
                    </ul>
                  </li>
                ))}
              </ul>
            ) : (
              <em>None loaded</em>
            )}
          </Column>
        </Row>
      </Card>
    </div>
  );
};
const mapStateToProps = ({ entities }) => {
  const { sites } = entities;
  return {
    loading: sites.loading,
    list: sites.list,
  };
};

const mapDispatchToProps = {
  sitesLoaded,
};

const ConnectedSites = connect(mapStateToProps, mapDispatchToProps)(Sites);
export { ConnectedSites as Sites };
