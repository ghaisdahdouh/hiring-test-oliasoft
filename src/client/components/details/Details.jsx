import React, { useEffect, useState } from "react";
import { connect } from "react-redux";
import { useParams } from "react-router-dom";
import { Spinner } from "@oliasoft-open-source/react-ui-library";

const Details = ({ sites, loading, oilRigsLoaded }) => {
  const { siteId } = useParams();
  const [siteDetails, setSiteDetails] = useState();

  useEffect(() => {
    if (sites && sites.list) {
      const site = sites.list.find((site) => site.id.toString() === siteId);
      setSiteDetails(site);
    }
  }, [sites, siteId]);

  return (
    <div style={{ padding: 40 }}>
      <h2>Site Details</h2>
      {siteDetails ? (
        <div>
          <h3>{siteDetails.name}</h3>
          <p>
            <strong>Country:</strong> {siteDetails.country}
          </p>
          <ul>
            {siteDetails.oilRigs.map((rig) => (
              <li key={rig}>{rig}</li>
            ))}
          </ul>
        </div>
      ) : (
        <Spinner dark />
      )}
    </div>
  );
};

const mapStateToProps = (state) => {
  return {
    sites: state.entities.sites,
  };
};

export default connect(mapStateToProps)(Details);
